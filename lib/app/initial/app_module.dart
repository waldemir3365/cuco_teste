import 'package:cuco_teste/app/modules/home/home_module.dart';
import 'package:cuco_teste/app/modules/profile/profile_module.dart';
import 'package:cuco_teste/app/modules/splash/splash_module.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppModule extends Module {
  @override
  final List<Bind> binds = [

  ];

  @override
  final List<ModularRoute> routes = [
    ModuleRoute('/splash', module: SplashModule(),transition: TransitionType.upToDown),
    ModuleRoute('/home', module: HomeModule(),transition: TransitionType.fadeIn),
    ModuleRoute('/profile', module: ProfileModule(),transition: TransitionType.fadeIn),
  ];
}
