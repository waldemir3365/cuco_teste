import 'name_model.dart';
import 'picture_model.dart';

class UserModel{
  UserModel({
    required this.gender,
    required this.name,
    required this.email,
    required this.picture,
  });

  String gender;
  Name name;
  String email;
  Picture picture;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    gender: json["gender"],
    name: Name.fromJson(json["name"]),
    email: json["email"],
    picture: Picture.fromJson(json["picture"]),
  );

}
