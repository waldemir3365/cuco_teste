class Picture {
  Picture({
    required this.large,
  });

  String large;

  factory Picture.fromJson(Map<String, dynamic> json) => Picture(
    large: json["large"],
  );
}
