import 'dart:convert';

import 'package:cuco_teste/app/models/user_model.dart';


class UserListModel {
  List<UserModel> results;

  UserListModel({
    required this.results,
  });

 static UserListModel usersFromJson(String str) => UserListModel.fromJson(json.decode(str));

  factory UserListModel.fromJson(Map<String, dynamic> json) => UserListModel(
    results: List<UserModel>.from(json["results"].map((x) => UserModel.fromJson(x))),
  );

}

