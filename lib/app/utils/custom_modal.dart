import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class CustomModal{

  static void showAlertDialog(BuildContext context) {
    Widget continueButton = TextButton(
      child: Text("Ok"),
      onPressed:()=> Modular.to.pop(),
    );

    AlertDialog alert = AlertDialog(
      title: Text("Atenção"),
      content: Text("Ouve um problema com sua Internet ! Tente mais Tarde"),
      actions: [
        continueButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
