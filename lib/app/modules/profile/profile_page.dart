import 'package:cuco_teste/app/components/custom_avatar.dart';
import 'package:cuco_teste/app/components/custom_scaffold.dart';
import 'package:flutter/material.dart';
class ProfilePage extends StatelessWidget {

  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(isAppBar: true, widget: bodyPage(), isProfile: false,title: 'Profile');
  }

  Widget bodyPage(){
    return SafeArea(
        minimum: const EdgeInsets.only(top: 20,bottom: 20),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const CustomAvatar(raidus: 100),
              const SizedBox(height: 30,),
              customRowNames('Waldemir Gomes'),
              customRowNames('waldemir.gomes93@gmail.com'),
            ],
          ),
        )
    );
  }
  Widget customRowNames(String name){
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text('$name',style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold)),
      ],
    );
  }
}
