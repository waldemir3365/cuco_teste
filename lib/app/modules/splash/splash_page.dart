import 'package:cuco_teste/app/components/custom_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  @override
  void initState() {
    Future.delayed(Duration(seconds: 4),()=> Modular.to.pushNamed('/home/'));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(isAppBar: false, widget:bodyPage(),isProfile: false);
  }

  Widget bodyPage(){
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: const [
          FlutterLogo(
            size:200,
          )
        ],
      ),
    );
  }
}
