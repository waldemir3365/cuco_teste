// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  final _$listModelAtom = Atom(name: '_HomeControllerBase.listModel');

  @override
  UserListModel get listModel {
    _$listModelAtom.reportRead();
    return super.listModel;
  }

  @override
  set listModel(UserListModel value) {
    _$listModelAtom.reportWrite(value, super.listModel, () {
      super.listModel = value;
    });
  }

  final _$isLoadingAtom = Atom(name: '_HomeControllerBase.isLoading');

  @override
  bool get isLoading {
    _$isLoadingAtom.reportRead();
    return super.isLoading;
  }

  @override
  set isLoading(bool value) {
    _$isLoadingAtom.reportWrite(value, super.isLoading, () {
      super.isLoading = value;
    });
  }

  final _$isLoadingListAtom = Atom(name: '_HomeControllerBase.isLoadingList');

  @override
  bool get isLoadingList {
    _$isLoadingListAtom.reportRead();
    return super.isLoadingList;
  }

  @override
  set isLoadingList(bool value) {
    _$isLoadingListAtom.reportWrite(value, super.isLoadingList, () {
      super.isLoadingList = value;
    });
  }

  final _$fetchApiAsyncAction = AsyncAction('_HomeControllerBase.fetchApi');

  @override
  Future fetchApi(int page) {
    return _$fetchApiAsyncAction.run(() => super.fetchApi(page));
  }

  @override
  String toString() {
    return '''
listModel: ${listModel},
isLoading: ${isLoading},
isLoadingList: ${isLoadingList}
    ''';
  }
}
