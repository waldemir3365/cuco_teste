import 'package:connectivity/connectivity.dart';
import 'package:cuco_teste/app/components/custom_scaffold.dart';
import 'package:cuco_teste/app/modules/home/home_controller.dart';
import 'package:cuco_teste/app/utils/custom_modal.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage,HomeController> {

  @override
  void initState(){
   controller.fetchApi(controller.counter);
   controller.scrollListener();
    super.initState();
  }

  @override
  void dispose() {
    controller.scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(isAppBar: true, widget:bodyPage(context) ,title: 'Home',isProfile: true,hasImplicity: false);
  }

  Widget bodyPage(BuildContext context){
    return WillPopScope(
      onWillPop:()async =>false,
        child: SafeArea(
          minimum: EdgeInsets.all(10),
          child: Observer(
            builder: (context){
              return Column(
                children: [
                  Expanded(
                      child:controller.isLoading ? loadPage() :
                      ListView.builder(
                        controller: controller.scrollController,
                          itemCount: controller.listModel.results.length,
                          itemBuilder:(context, index){
                            return itensList(index);
                          }
                      ),
                  ),
                 controller.isLoadingList ? CircularProgressIndicator():SizedBox(),
                ],
              );
            }
          )
        )
    );
  }

  Widget loadPage(){
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget itensList(int index){
    return Card(
      elevation: 5,
      child: ListTile(
        leading: CircleAvatar(
          backgroundImage: NetworkImage(controller.listModel.results[index].picture.large),
        ),
        title: Text(controller.listModel.results[index].name.fullName),
        subtitle: Text(controller.listModel.results[index].email),
      ),
    );
  }
}
