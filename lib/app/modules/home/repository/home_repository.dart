import 'package:cuco_teste/app/models/user_list_model.dart';

import 'interfaces/home_interface.dart';
import 'package:http/http.dart' as http;

class HomeRepository implements IHomeRepository {
  @override
  Future<UserListModel> getListUsers(int page)async {
    final response = await http.get(Uri.parse('https://randomuser.me/api/?page=$page&results=15'));
    if (response.statusCode == 200) {
      return UserListModel.usersFromJson(response.body);
    } else {
      return throw Exception("Nao foi possivel efetuar operacao. !");
    }
  }
}