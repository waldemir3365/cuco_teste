import 'package:cuco_teste/app/models/user_list_model.dart';

abstract class IHomeRepository{
  Future<UserListModel> getListUsers(int page);
}