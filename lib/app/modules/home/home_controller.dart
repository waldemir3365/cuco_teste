import 'package:cuco_teste/app/models/user_list_model.dart';
import 'package:cuco_teste/app/utils/custom_modal.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';
import 'repository/interfaces/home_interface.dart';

part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  final IHomeRepository _repository;
  final ScrollController scrollController = ScrollController();

  _HomeControllerBase(this._repository);

  @observable
  UserListModel listModel = UserListModel(results: []);

  @observable
  bool isLoading = true;

  @observable
  int counter = 0;

  @observable
  bool isLoadingList = false;

  @action
  fetchApi(int page) async {
    await _repository.getListUsers(page).then((value) {
      listModel.results.addAll(value.results);
    });
    isLoading = false;
    isLoadingList = false;
  }
  @action
  scrollListener(){
    scrollController.addListener(() {
      if(scrollController.position.pixels == scrollController.position.maxScrollExtent){
          isLoadingList = true;
          fetchApi(counter++);
      }
    });
  }
}
