import 'package:cuco_teste/app/components/custom_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class CustomScaffold extends StatelessWidget {
  final bool isAppBar;
  final Widget widget;
  final String? title;
  final bool isProfile;
  final bool? hasImplicity;
  const CustomScaffold({Key? key,required this.isAppBar,required this.widget,this.title,required this.isProfile, this.hasImplicity}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(isProfile:isProfile,isAppBar:isAppBar,hasImplicity:hasImplicity,title:title),
      body: widget,
    );
  }

}
