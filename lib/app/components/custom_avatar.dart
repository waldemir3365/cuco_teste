import 'package:flutter/material.dart';

class CustomAvatar extends StatelessWidget {

  final double raidus;
  const CustomAvatar({Key? key, required this.raidus}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: raidus,
      backgroundImage: AssetImage('assets/images/natureza.jpg'),
    );
  }
}
