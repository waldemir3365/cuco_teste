import 'package:cuco_teste/app/components/custom_avatar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  final bool isAppBar;
  final String? title;
  final bool isProfile;
  final bool? hasImplicity;

  const CustomAppBar({Key? key, required this.isAppBar, this.title, required this.isProfile, this.hasImplicity}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return isAppBar ? appBar(): Container();
  }

  @override
  Size get preferredSize => const Size.fromHeight(60);

  Widget appBar(){
    return AppBar(
      title: Text(title ?? ''),
      automaticallyImplyLeading: hasImplicity ?? true,
      centerTitle: true,
      actions:[
        isProfile ? Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            onTap: ()=> Modular.to.pushNamed('/profile/'),
            child:const CustomAvatar(raidus: 35),
          ),
        ) : const SizedBox(),
      ],
    );
  }
}
